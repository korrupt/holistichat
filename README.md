# HolistiChat by Holistic Coding
> Anonymous/Sensitive Chatting

HolistiChat is an end-to-end RSA 3072 bit encrypted chat client that can (optionally) be hosted and used over tor.
If you don't want to use tor just use ngrok and your clients may still use tor for their connections.

You may also re-generate your keys with rsa.py.
If you do re-generate be sure to share the new keys with other users prior to client launching.



## Installation

Run the following in the torp2p directory. 

For Server Hosting:
```
sudo pip3 install -r requirements.txt
python3 server.py
```

For clients:
```
sudo pip3 install -r requirements.txt
python3 holistichat.py
```


> It is recommended to host through ngrok, or forward ports and host through a tor onion.

~~~
 Directories: - [ ] incl
                    - [ ] server.py - Main functions for server hosting
                    - [ ] main.py - Main functions for clients
                    - [ ] usr.csv - User Prefs
                    - [ ] logo.png - Icon
                    - [ ] rsa.py - Re-generate keys
                    - [ ] logo.py - Styling for rsa.py
                    - [ ] privated.pem - key
                    - [ ] private.pem - key (null, will be used for exchange)
                    - [ ] public.pem - key
              -  [ ] server.py - Server launcher
              -  [ ] holistichat.py - Client launcher
More Coming Soon!
      Recomendations Welcomed
~~~




Tor P2P And Chat Server / Client.