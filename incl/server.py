import socket
import socks
from queue import Queue
from time import sleep
from threading import Thread, Lock

##########################################
####  HolistiChat By Holistic Coding  ####
####     SoftExploit and Korrupt      #### 
####     	 L3773R5              #### 
####  Contributors:                   #### 
####        Freesec, Neo, Nix         ####
##########################################


## Color Coding
class BC:
    G = '\033[92m'
    A = '\033[0;37m'
    F = '\033[91m'
    B = '\033[1m'
    E = '\033[0m'
    
## Main Server Function
class TorServ:
    
    def __init__(self):
    ## Edit to your liking
        self.hoster = ["korrupt"]
        self.hostee = ["korrupt"]
        self.userinfo = {
            "korrupt" : "r00t"
        }
        #self.password = ["r00t"]
        self.port = 5001

    ## Don't touch
        self.con = ""
        self.host = '127.0.0.1'
        self.ThreadCount = 0
        self.q = Queue()
        self.clients = []
        self.ping = []  
        self.pingn = []  
        self.kill = []  
        self.named = []
        self.clients = []
        self.q = Queue()
        self.usernames = []
        self.passwords = []
        self.lock = Lock()
        #self.qme("Welcome")
        
    ## Connect
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.bind(('0.0.0.0', self.port))
        self.s.listen(500)
        try:
            self.s.bind((self.host, self.port))
        except socket.error as e:
            print(str(e))
        self.s.listen(5)
        print('> Locked And Loaded.')
        print('> Waiting For Connections...')
        self.data = ""
    
    ## Sending Data
    def sending(self):
        self.dat = self.q.get()
        for i in self.clients:
            if self.con != i:
                try:
                    i.send(self.dat)
                    print("Sent to " + str(i))
                    print(str(self.dat))
                except OSError:
                    pass
    
    
    def qme(self, message):
        with self.lock:
            self.q.put(message)
            print(message)
            Thread(target=self.sending).start()
            #self.sending()
            #sleep(1)
    
    def addremove(self, user):
        for i in self.clients:
            try:
                i.send(user.encode('utf-8'))
                print("Sent to " + str(i))
                print(user)
            except OSError:
                pass
    
    
    ## Each users thread
    def threaded_user(self, connection):
        ## Receive the name of the user
        name = connection.recv(2048).decode('utf-8')
        password = connection.recv(2048).decode('utf-8')
        add_user = "+" + name
        Fault = False
        print(add_user)
        if name not in self.usernames:
            self.usernames.append(name)
            self.userinfo.update({name: password})
            self.named.append(name.lower())
            connection.send(str(self.named).encode('utf-8'))
            self.conn = "User " + BC.F + name + BC.E + " has connected."
            self.qme(self.conn.encode('utf-8'))
        elif name in self.usernames:
            password_check = self.userinfo.get(name)
            if password != password_check:
                connection.send("WRONG PASSWORD".encode('utf-8'))
                print("WRONG")
                sleep(1)
                Fault = True
                connection.close()
            else:
                connection.send(str(self.named).encode('utf-8'))
                self.named.append(name.lower())
                self.conn = "User " + BC.F + name + BC.E + " has connected."
                self.qme(self.conn.encode('utf-8'))
        
        #self.qme(str("+" + name).encode('utf-8'))
        sleep(1)
        if Fault != True:
            #connection.send(str("+" + name).encode('utf-8'))
            #self.sending()
            joined = "User " + name + " has connected."
            #self.qme(add_user.encode('utf-8'))
            #self.sending()
            print(joined)
            self.clients.append(connection)
            self.addremove(add_user)
            self.chck = False
            try:
                #self.sending()
        ## Main User Thread Loop
                while True:
                    sleep(.5)
                    try:
                        self.data = connection.recv(2048)
                        print(name)
                        self.con = connection
                        self.br = self.data.decode('utf-8')
                    except OSError:
                        left = "-" + name
                        self.qme(left.encode('utf-8'))
                        #self.sending()
                        connection.close()
                        break
        ## Disconnect
                    if not self.data:
                        try:
                            self.clients.remove(connection)
                            left = "-" + name
                            self.qme(left.encode('utf-8'))
                            #self.sending()
                            self.named.remove(name)
                            connection.close()
                            break
                        except:
                            pass
                        left = "-" + name
                        self.qme(left.encode('utf-8'))
                        #self.sending()
                        connection.close()
                        self.dat = 'User ' + BC.F + name + BC.E + " left."
                        self.qme(self.dat.encode('utf-8'))
                        print(self.dat)
        ## List Users
                    elif self.br.lower() == "!list":
                        self.users = "Current Users: " + BC.F + ', '.join(self.named) + BC.E
                        print(self.users)
                        connection.send(self.users.encode('utf-8'))
        ## Private message
                    elif self.br[:1] == "@":
                        self.a = self.br.split()[0]
                        try:
                            self.clients[self.named.index(self.a[1:].lower())].send(self.br.encode('utf-8'))
                        except ValueError:
                            self.notfound = "User " + self.a[1:] + " not found."
                            connection.send(str(self.notfound).encode('utf-8'))
                    elif self.br[:4].lower() == "!del":
                        print(self.br)
                        print(self.usernames.index(self.br[5:]))
                        self.usernames.pop(self.usernames.index(self.br[5:]))
                        self.userinfo.pop(self.br[5:])
        ## Kick user
                    elif self.br[:5].lower() == "!kick":
                        print(self.kill)
                        if name.lower() in self.hoster:
                            self.kick = self.br.split()[1]
                            print(self.kick)
                            print(self.br)
                            if self.kick not in self.hoster:
                                self.kicked = "You have been kicked!"
                                try:
                                    self.clients[self.named.index(self.kick)].send(self.kicked.encode('utf-8'))
                                    self.clients[self.named.index(self.kick)].close()
                                    self.named.pop(self.kick)
                                except (ValueError):
                                    invalid = "Invalid User."
                                    connection.send(invalid.encode('utf-8'))
                                except:
                                    self.kicked = "-" + self.kick
                                    self.qme(self.kicked.encode('utf-8'))
                                sleep(.5)
                                self.kicker = BC.F + self.kick + BC.E + " has been kicked by " + BC.F + name + BC.E + "."
                                self.qme(str(self.kicker).encode('utf-8'))
                                # try:
                                #     self.password.pop[int(self.usernames.index(self.kick))]
                                #     self.usernames.pop(self.kick)
                                # except:
                                #     pass
                                #self.sending()
                                left = "-" + self.kick
                                self.qme(left.encode('utf-8'))
                        else:
                            self.unauth = name + " attempted an unauthorized command."
                            self.qme(self.unauth.encode('utf-8'))
                            #self.sending()
        ## Help menu
                    elif self.br[:5].lower() == "!help":
                        if name.lower() in self.hoster:        
                            self.help1 = BC.F + "Root: @user message " + BC.E + "- send someone a private message"
                            self.help2 = BC.F + "Root: !list " + BC.E + "- show current users"
                            self.help3 = BC.F + "Root: !admins " + BC.E + "- show current admins"
                            self.help4 = BC.F + "Root: !add " + BC.E + "- add an admin"
                            self.help5 = BC.F + "Root: !remove " + BC.E + "- remove an admin"
                            self.help6 = BC.F + "Root: !kick " + BC.E + "- kick someone"
                            self.help = [self.help1, self.help2, self.help3, self.help4, self.help5, self.help6]
                        else:
                            self.help1 = BC.F + "Root: @user message " + BC.E + "- send someone a private message"
                            self.help2 = BC.F + "Root: !list " + BC.E + "- show current users"
                            self.help3 = BC.F + "Root: !admins " + BC.E + "- show current admins"
                            self.help = [self.help1, self.help2, self.help3]
                        for i in self.help:
                            connection.send(i.encode('utf-8'))
                            sleep(.5)
        ## Add an admin
                    elif self.br[:4].lower() == "!add":
                        if name.lower() in self.hoster:
                            self.hoster.append(self.br[5:])
                            self.hosters = "Admins: " + BC.F + ', '.join(self.hoster) + BC.E
                            connection.send(self.hosters.encode('utf-8'))
                        else:
                            self.nf = "User: You are not admin. Type !admins to find one.\n"
                            connection.send(self.nf.encode('utf-8'))
                            self.nf = name + " attempted an unauthorized command."
                            self.qme(self.nf.encode('utf-8'))
                            #self.sending()
        ## Remove an admin
                    elif self.br[:7].lower() == "!remove":
                        if name.lower() in self.hoster:
                            try:
                                self.hoster.remove(self.br[8:])
                                self.hosters = "Admins: " + BC.F + ', '.join(self.hoster) + BC.E
                                connection.send(self.hosters.encode('utf-8'))
                            except:
                                self.nou = "Admins: No user named " + BC.F + self.br[8:] + BC.E + " in admins."
                                connection.send(self.nou.encode('utf-8'))
                        else:
                            self.nf = "User: You are not admin. Type !admins to find one.\n"
                            connection.send(self.nf.encode('utf-8'))
                            self.nf = "User " + name + " attempted an administrative command."
                            self.qme(self.nf.encode('utf-8'))
                            #self.sending()
        ## List all admins
                    elif self.br[:7].lower() == "!admins":
                        self.hosters = "Admins: " + BC.F + ', '.join(self.hoster) + BC.E
                        connection.send(self.hosters.encode('utf-8'))
                    else:
        ## Send received data to all users
                        self.qme(self.data)
                        print(name + ": " + self.data.decode('utf-8'))
                        #self.sending()
            except (ConnectionResetError, BrokenPipeError):
                left = "-" + name
                self.qme(left.encode('utf-8'))
                #self.sending()
                self.clients.remove(connection)
                self.dat = 'User ' + BC.F + name + BC.E + " left."
                self.qme(self.dat.encode('utf-8'))
                #self.sending()
                if name in self.hoster:
                    self.hostee.append(name)
                
            
    def main(self):
    ## Convert hoster and hostee list to lowercase lettering
        for i in range(len(self.hostee)):
            self.hostee[i] = self.hostee[i].lower()
        for i in range(len(self.hoster)):
            self.hoster[i] = self.hoster[i].lower()
    ## Thread main chat area
        #Thread(target=self.sending).start()
        while True:
            try:
    ## Connections
                self.Client, self.address = self.s.accept()
    ## Threads for each new user
                Thread(target = self.threaded_user, args =(self.Client,)).start()
                self.ThreadCount += 1
                print('Thread Number: ' + str(self.ThreadCount) + " has started.")
            except:
                pass
                #self.s.close()
