from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import binascii
import sys
import os
from logo import *
import pyperclip as pc
import os
from subprocess import call

global acp
acp = ""

def clear():  
    _ = call('clear' if os.name =='posix' else 'cls') 

def generate():
    keyPair = RSA.generate(3072)

    pubKey = keyPair.publickey()
    print(pubKey)
    print(f"Public key:  (n={hex(pubKey.n)}, e={hex(pubKey.e)})")
    pubKeyPEM = pubKey.exportKey()
    print(pubKeyPEM.decode('ascii'))
    with open('public.pem', 'w') as f:
        f.write(pubKeyPEM.decode('ascii'))


    print(f"Private key: (n={hex(pubKey.n)}, d={hex(keyPair.d)})")
    privKeyPEM = keyPair.exportKey()
    print(privKeyPEM.decode('ascii'))
    with open ('privated.pem', 'w') as f:
        f.write(privKeyPEM.decode('ascii'))
    clear()
    logo2()
    start()
        
        
def encrypt(msg):
    global encrypted
    encryptor = PKCS1_OAEP.new(pubKey)
    encrypted = encryptor.encrypt(msg)
    encrypt = binascii.hexlify(encrypted)
    encrypt = encrypt.decode()
    clear()
    logo2()
    print(BC.G + "")
    print(BC.G + "Encrypted:", encrypt)
    print(BC.G + "")
    if acp == "1":
        print(BC.G + "** Text was copied to clipboard.")
        print(BC.G + "")
        pc.copy(encrypt)
    main()
            

def decrypt(mssg):
    msg = binascii.unhexlify(mssg)
    decryptor = PKCS1_OAEP.new(privKeyPEM)
    decrypted = decryptor.decrypt(msg)
    decrypted = decrypted.decode()
    clear()
    logo2()
    print(BC.G + "")
    print('Decrypted:', decrypted)
    print(BC.G + "")
    main()



def start():
    global mykey
    global prkey
    global pkey
    global pubKey
    global privKeyPEM
    global dv
    clear()
    dv = ""
    os.system('touch public.pem')
    os.system('touch private.pem')
    os.system('touch privated.pem')
    with open('public.pem') as f:
        pkey = f.read()
    if pkey == "":
        pass
    else:
        print(BC.G + "Read Public Key As: ")
        print(pkey)
        pubKey = RSA.importKey(pkey)
        #pubKey = int(pubKey)
        print(pubKey)
    with open('privated.pem') as f:
        mykey = f.read()
    if mykey == "":
        pass
    else:
        print(BC.G + "Read Your Private Key As: ")
        print(mykey)
        print(BC.G + "")
    with open('private.pem') as f:
        prkey = f.read()
    if prkey == "":
        pass
    else:
        print(BC.G + "Read Current Decrypt Private Key As: ")
        print(prkey)
        print(BC.G + "")
        try:
            privKeyPEM = RSA.importKey(prkey)
            #pubKey = int(pubKey)
            print(privKeyPEM)
        except ValueError:
            dv = "0"
            main()
    logo2()
    main()
    
    
def importing():
    no_of_lines = 39
    lines = ""
    print(BC.G + "Paste Your Key [include header and footer]")
    try:
        for i in range(no_of_lines):
            lines+=input()+"\n"
        with open('private.pem', 'w') as f:
            f.write(lines)
    except (RuntimeError, TypeError, NameError, ValueError):
        print(BC.G + "That didn't work! Re-copy their key and retry.")
    logo2()
    start()

def main(): 
    global acp
    print(BC.G + "")
    if pkey == "":
        print(BC.G + "[ ] Personal Private/Public Key Not Generated.")
    else:
        print(BC.G + "[*] Personal Private/Public Key Loaded.")
    if prkey == "":
        print(BC.G + "[ ] Decryptable Private Key Not Imported.")
    elif dv == "0":
        print(BC.G + "** Decryptable Key Imported With Errors, Try Again.")
    else:
        print(BC.G + "[*] Decryptable Private Key Loaded.")
    if acp == "1":
        print(BC.G + "[*] Auto Copy/Paste")
    else:
        print(BC.G + "[ ] Auto Copy/Paste")
    
    print(BC.G + "")
    mitems = ["Encrypt", "Decrypt", "Generate", "Import A Private Key", "Copy Your Personal Private Key", "Print Keys", "Auto Copy/Paste Encrypt And Decrypt", "Quit"]
    for idx, i in enumerate(mitems, start=1):
        print( BC.G + " [" + BC.F + str(idx) + BC.G + "] " + i)
    print("")
    print("")
    print(BC.E + "   Enter An Item Number" + BC.G)
    sys.stdout.write("\033[F")
    q = input(BC.G + ">" + BC.F)
    print(BC.G + "")
    
    if q == "1":
        if pkey == "":
            print(BC.G + "ERROR:")
            print(BC.G + "Please Generate A Key First.")
            print(BC.G + "")
            main()
        else:
            global lines
            lines = ""
            howmany = input("How many lines are going to be encrypted: ")
            try:
                howmany = int(howmany)
                enc = print("Enter A String: ") 
                for i in range(howmany):
                    lines+=input()+"\n"
                lines = bytes(lines, 'utf-8') 
                be = encrypt(lines)
            except ValueError:
                clear()
                print("That Wasn't A Valid Number.")
                main()
    elif q == "2":
        if prkey == "":
            print(BC.G + "ERROR:")
            print(BC.G + "Please Import The Users Private Key First.")
            print(BC.G + "")
            main()
        else:
            try:
                #dec = input("Encrypted String To Decrypt: ")
                if acp == "1":
                    dec = pc.paste()
                else:
                    dec = input("Input String To Decrypt:")
                dec = bytes(dec, 'utf-8') 
                decrypt(dec)
            except ValueError:
                print(BC.G + "")
                print(BC.G + "**The Text Entered Did Not Work With The Decrypt Key.")
                print(BC.G + "")
            main()
    elif q == "3":
        generate()
    elif q == "4":
        importing()
    elif q == "5":
        pc.copy(mykey)
        main()
    elif q == "6":
        if pkey == "":
            print("No Private/Public Keys To Show.")
        else:
            print(pkey)
            print(BC.G + "")
            print(BC.G + "Your Private Key For Others To Decrypt: ")
            print(mykey)
            print("")
        if prkey == "":
            print("No Decryptable Key To Show.")
        else:
            print(prkey)
            print(BC.G + "")
        logo2()
        main()
    elif q == "7":
        clear()
        if acp == "":
            acp = "1"
        elif acp =="1":
            acp = ""
        logo2()
        main()
    elif q == "8":
        sys.exit()
    else:
        main()
start()
