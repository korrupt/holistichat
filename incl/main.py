from PyQt5 import QtCore, QtGui, QtWidgets
import socket
import socks
from queue import Queue
from threading import Thread
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import binascii
from time import sleep
from subprocess import call
import readline
import time
import subprocess
import notify2
import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import faulthandler
import platform

##########################################
####  HolistiChat By Holistic Coding  ####
####     SoftExploit and Korrupt      #### 
####     	     L3773R5              #### 
####  Contributors:                   #### 
####        Freesec, Neo, Nix         ####
##########################################


## Color Coding
class BC:
    G = '\033[92m'
    A = '\033[0;37m'
    F = '\033[91m'
    B = '\033[1m'
    E = '\033[0m'

## Main Function
class Torchat:

    def __init__(self):
        faulthandler.enable()
        global TorMessenger
        global TorMessengerLogin
        self.notified = 0
        self.socks_port = 9050
        self.systemInfo = platform.system()
        self.usrImport()
        app = QtWidgets.QApplication(sys.argv)
        app.setStyle("Fusion")
        dark_palette = QPalette()
        dark_palette.setColor(QPalette.Window, QColor(53, 53, 53))
        dark_palette.setColor(QPalette.WindowText, Qt.white)
        dark_palette.setColor(QPalette.Base, QColor(25, 25, 25))
        dark_palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
        dark_palette.setColor(QPalette.ToolTipBase, Qt.white)
        dark_palette.setColor(QPalette.ToolTipText, Qt.white)
        dark_palette.setColor(QPalette.Text, Qt.white)
        dark_palette.setColor(QPalette.Button, QColor(53, 53, 53))
        dark_palette.setColor(QPalette.ButtonText, Qt.white)
        dark_palette.setColor(QPalette.BrightText, Qt.red)
        dark_palette.setColor(QPalette.Link, QColor(42, 130, 218))
        dark_palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
        dark_palette.setColor(QPalette.HighlightedText, Qt.black)
        app.setPalette(dark_palette)
        app.setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }")
        TorMessengerLogin = QtWidgets.QDialog()
        TorMessenger = QtWidgets.QMainWindow()
        ui2 = self
        ui2.setupUi2(TorMessenger)
        #TorMessenger.show()
        ui = self
        ui.setupUi(TorMessengerLogin)
        TorMessengerLogin.show()
        try:
            sys.exit(app.exec_())
        except:
            pass

    ## Tor
    def tor(self):
        try:
            global users_list
            socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", int(self.TorPort.text()), True)
            self.s = socks.socksocket()
            self.s.connect((self.ServerURL.text(), int(self.ChatPort.text())))
            sleep(1)
            self.s.send(self.name.encode('utf-8'))
            sleep(1)
            self.s.send(self.password.encode('utf-8'))
            sleep(1)
        except (socks.ProxyConnectionError, socket.gaierror, BrokenPipeError, socks.GeneralProxyError):
            self.label_status.setText("Status: Failed.")
            QApplication.processEvents()
    
    ## Clearnet
    def clearnet(self):
        try:
            global users_list
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.connect((self.ServerURL.text(), int(self.ChatPort.text())))
            sleep(1)
            self.s.send(self.name.encode('utf-8'))
            sleep(1)
            self.s.send(self.password.encode('utf-8'))
            sleep(1)
        except (socks.ProxyConnectionError, socket.gaierror, BrokenPipeError):
            self.label_status.setText("Status: Failed.")
            QApplication.processEvents()

    ## Initial Questions
    def questioning(self):
        if self.torme == "y":
            self.tor()
        else:
            self.clearnet()
        ThreadCount = 0
        self.q = Queue()
        self.data = "" 
        self.threaded_user(self.s)

    ## Timestamp
    def timed(self):
        self.secondsSinceEpoch = time.time()
        self.timeObj = time.localtime(self.secondsSinceEpoch)
        self.timer = "[" + str(self.timeObj.tm_hour) + ":" + str(self.timeObj.tm_min) + ":" + str(self.timeObj.tm_sec) + "]"   
       
    ## Encrypt Messages
    def encrypt(self, msg):
        encryptor = PKCS1_OAEP.new(self.pubKey)
        self.encrypted = encryptor.encrypt(msg)
        encrypt = binascii.hexlify(self.encrypted)
        self.encrypted = encrypt.decode()

    ## Decrypt Messages
    def decrypt(self, mssg):
        try:
            msg = binascii.unhexlify(mssg)
            self.decryptor = PKCS1_OAEP.new(self.privKeyPEM)
            self.decrypted = self.decryptor.decrypt(msg)
            return self.decrypted.decode()
        except ValueError:
            pass
        
    def showme(self):
        TorMessengerLogin.hide()
        TorMessenger.show()

    ## Get keys
    def keys(self):
        if self.systemInfo == "Linux":
            with open('incl/public.pem') as f:
                self.pkey = f.read()
            if self.pkey == "":
                pass
            else:
                self.pubKey = RSA.importKey(self.pkey)
            with open('incl/privated.pem') as f:
                self.mykey = f.read()
            with open('incl/private.pem') as f:
                self.prkey = f.read()
        elif self.systemInfo == "Windows":
            with open('incl\\public.pem') as f:
                self.pkey = f.read()
            if self.pkey == "":
                pass
            else:
                self.pubKey = RSA.importKey(self.pkey)
            with open('incl\\privated.pem') as f:
                self.mykey = f.read()
            with open('incl\\private.pem') as f:
                self.prkey = f.read()
        if self.prkey == "":
            pass
        try:
            self.privKeyPEM = RSA.importKey(self.prkey)
        except ValueError:
            pass
      
    ## Notifications  
    def notify(self):
        try:
            notify2.init("Alert") 
            self.g = notify2.Notification(None)
            self.g.set_urgency(notify2.URGENCY_CRITICAL) 
            self.g.set_timeout(10000) 
        except AttributeError:
            self.notified = 1
        
    ## Initial start up
    def start(self):
        self.keys()
        self.questioning()
        self.notify()
    
    ## Send message to be encrypted, then send message
    def sending(self, jj):
        self.encrypt(jj.encode('utf-8'))
        try:
            self.s.send(self.encrypted.encode('utf-8'))
        except OSError:
            sys.exit(1)

    def messages_area(self, message):
        msg = str(message)
        msg = msg.replace("[91m", "")
        msg = msg.replace("[0m", "")
        msg = msg.replace("[92m", "")
        msg = msg.replace("[91m11[0m:[91m19[0m:[91m19[0m] [91m", "")
        msg = msg.replace("]", "")
        try:
            #while len(msg) > 0:
            self.timed()
            msg = self.timer + " " + msg
            self.ChatWindow.append(msg)
            self.ChatWindow.moveCursor(QtGui.QTextCursor.End)
        except:
            pass

    ## Main User Function
    ## For communication from the server
    def threaded_user(self, connection):
        global users_list
        Fault = False
        try:
            if Fault == False:
                users_list = self.s.recv(3072).decode('utf-8')
                if users_list == "WRONG PASSWORD":
                    self.label_status.setText("Status: Wrong Password.")
                    QApplication.processEvents()
                    self.s.close()
                else:
                    users_list = users_list.replace("'", "")
                    users_list = users_list.strip('][').split(', ') 
                    if users_list[0] == '':
                        users_list.pop(0)
                    users_list.insert(0,"--Users--")
                    if self.name not in users_list:
                        users_list.append(str(self.name))
                    if len(users_list) <= 1:
                        self.OnlineUserList.addItems(users_list)
                    elif len(users_list) > 1:
                        for i in users_list:
                            self.OnlineUserList.addItem(i)
                    Thread(target=self.connected, args=(self.s,)).start()
                    self.showme()
        except:
    ## Set fault so the login won't load the chat window
            Fault = True
            self.s.close()
    
    def connected(self, connection):
        self.ChatWindow.append("Welcome To The Server")
        while True:
            try:
    ## Receive Data From Server
                self.data = connection.recv(2048)
                    
            except OSError:
                sys.exit(0)
                return
            check = self.data.decode('utf-8')
    ## Various communication from server
            if check[:4] == "User":
                self.messages_area(check)
    ## Private messages
            elif check[:1] == "-":
                try:
                    users_list.remove(check[1:])
                    self.OnlineUserList.clear()
                    self.OnlineUserList.addItems(users_list)
                    self.messages_area(check[1:] + " left the chat.")
                    QApplication.processEvents()
                except ValueError:
                    pass
            elif check[:1] == "+":
                if check[1:] in users_list:
                    pass
                elif check[1:] == self.name + "+" + self.name:
                    pass
                else:
                    users_list.append(check[1:])
                    self.OnlineUserList.addItem(check[1:])
                    QApplication.processEvents()
            elif check[:1] == "@":
                a = check.split()[0]
                check = check.replace(a, '')
                self.decrypt(check[1:])
                check = a + self.decrypted.decode('utf-8')
                self.messages_area(check)
                if self.notified != 1:
                    self.g.update(check)
                    self.g.show()
    ## Current Users
            elif check[:7] == "Current":
                self.messages_area(check)
    ## Errors
            elif check[:5] == "Error":
                self.messages_area(check)
                print("Connection Terminated.")
                subprocess.run(['pkill', 'chat.py'])
    ## Password Check
            elif check[:8] == "Password":
                self.ms = input("Enter Password: ")
                self.ms = "Password " + self.ms
                self.s.send(self.ms.encode('utf-8'))
                self.messages_area(check)
    ## List Admins
            elif check[:6] == "Admins":
                self.messages_area(check)
    ## Kick Notice
            elif check == "You have been kicked!":
                self.messages_area(check)
                self.s.close()
                subprocess.run(['pkill', 'chat.py'])
            else:
                try:
    ## Decrypt messages, then display
                    self.decrypted = self.decrypt(self.data.decode('utf-8'))
                    if self.decrypted == None:
                        if self.name in self.data.decode('utf-8'):
                            self.parse = self.data.decode('utf-8')
                            self.parse = self.parse.replace('[0m', '')
                            self.parse = self.parse.replace('[92m', '')
                            self.parse = self.parse.replace('[91m', '')
                            self.parse = self.parse.replace('', '')
                            self.parse = self.parse.replace('[0m', '')
                            try:
                                if self.notified != 1:
                                    self.g.update(self.parse)
                                    self.g.show()
                                self.messages_area(self.data.decode('utf-8'))
                            except:
                                pass
                        else:
                            try:
                                self.messages_area(self.data.decode('utf-8'))
                            except:
                                pass
                    else:
                        if self.name in self.decrypted:
                            self.parse = self.decrypted
                            self.parse = self.parse.replace('[0m', '')
                            self.parse = self.parse.replace('[92m', '')
                            self.parse = self.parse.replace('', '')
                            self.parse = self.parse.replace('[91m', '')
                            self.parse = self.parse.replace('[0m', '')
                            if self.notified != 1:
                                self.g.update(self.parse)
                                self.g.show()
                            self.timed()
                            self.messages_area(self.decrypted)
                        else:
                            self.timed()
                            self.messages_area(self.decrypted)
                except NameError:
                    pass   
                except OSError:
                    sys.exit(0)
    
    ## Main function
    def main(self):
        try:
    ## Input area
            self.ms = self.CommentBox.text()
            self.CommentBox.clear()
        except (ValueError, OSError):
            sys.exit(0)
        self.timed()
        if self.ms == "":
            pass
        else:
            my_msg = BC.F + self.name + BC.E +  ": " + self.ms
            self.messages_area(my_msg)
        ## Character Cap
            if len(self.ms) >= 240:
                err = BC.F + "Your message must be below 200 characters. Not sent." + BC.E
                self.messages_area(err)
        ## Command communication to the server
            elif self.ms.lower() == "exit":
                self.s.send(self.ms.encode('utf-8'))
                self.s.close()
                subprocess.run(['pkill', 'chat.py'])  
            elif self.ms[:1] == "!":
                self.s.send(self.ms.encode('utf-8'))
            elif self.ms[:1] == "@":
                a = self.ms.split()[0]
                self.ms = self.ms + " [From " + self.name + "]"
                self.ms = self.ms.replace(a, '')
                self.encrypt(self.ms.encode('utf-8'))
                self.ms = a + ' ' + self.encrypted
                self.s.send(self.ms.encode('utf-8'))
            elif self.ms[:5].lower() == "!help":
                self.s.send(self.ms.encode('utf-8'))
            else:
        ## Send message to the server (encrypt and send -- in the sending function)
                self.ms= BC.G + self.name + BC.E + ": " + self.ms
                self.sending(self.ms)

    def initing(self):
    ## Thread for receiving messages
        Thread(target=self.threaded_user, args=(self.s,)).start()
        
    ## Saving user login info (except pass)
    def usrSave(self):
        with open("incl/usr.csv", "w") as f:
            for i in self.userInfo:
                f.write(i + "\n")


    ## Importing user login info on start
    def usrImport(self):
        self.usrInfo = []
        with open("incl/usr.csv", "r") as f:
            self.usrInfo = f.readlines()
        self.usrInfo = [x.strip() for x in self.usrInfo]


    ## QT Login Area
    def setupUi(self, TorMessengerLogin):
        TorMessengerLogin.setObjectName("HolistiChat by Holistic Coding | Login")
        TorMessengerLogin.resize(399, 208)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("incl/logo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        TorMessengerLogin.setWindowIcon(icon)
        TorMessengerLogin.setStyleSheet("")
        self.gridLayout = QtWidgets.QGridLayout(TorMessengerLogin)
        self.gridLayout.setObjectName("gridLayout")
        self.label_torport = QtWidgets.QLabel(TorMessengerLogin)
        self.gridLayout.addWidget(self.label_torport, 4, 3, 1, 1)
        self.TOR = QtWidgets.QCheckBox(TorMessengerLogin)
        self.TOR.setObjectName("TOR")
        if "torTrue" in self.usrInfo:
            self.TOR.setChecked(True)
        else:
            pass
        self.gridLayout.addWidget(self.TOR, 5, 4, 1, 1)
        self.label_torport.setObjectName("label_torport")
        self.UserName = QtWidgets.QLineEdit(TorMessengerLogin)
        self.UserName.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.UserName.setObjectName("UserName")
        self.UserName.setMaxLength(25)
        self.UserName.setText(self.usrInfo[0])
        self.gridLayout.addWidget(self.UserName, 1, 1, 1, 4)
        self.Password = QtWidgets.QLineEdit(TorMessengerLogin)
        self.Password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.Password.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.Password.setObjectName("Password")
        self.Password.setMaxLength(16)
        self.gridLayout.addWidget(self.Password, 2, 1, 1, 4)
        self.ServerURL = QtWidgets.QLineEdit(TorMessengerLogin)
        self.ServerURL.setObjectName("ServerURL")
        self.gridLayout.addWidget(self.ServerURL, 3, 1, 1, 4)
        self.ServerURL.setText(self.usrInfo[1])
        self.ChatPort = QtWidgets.QLineEdit(TorMessengerLogin)
        self.ChatPort.setObjectName("ChatPort")
        self.gridLayout.addWidget(self.ChatPort, 4, 1, 1, 1)
        self.ChatPort.setText(self.usrInfo[2])
        self.label_status = QtWidgets.QLabel(TorMessengerLogin)
        self.label_status.setMaximumSize(QtCore.QSize(16777215, 30))
        self.label_status.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_status.setObjectName("label_status")
        self.gridLayout.addWidget(self.label_status, 5, 0, 1, 4)
        self.label_password = QtWidgets.QLabel(TorMessengerLogin)
        self.label_password.setMaximumSize(QtCore.QSize(16777215, 30))
        self.label_password.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_password.setObjectName("label_password")
        self.gridLayout.addWidget(self.label_password, 2, 0, 1, 1)
        self.label_username = QtWidgets.QLabel(TorMessengerLogin)
        self.label_username.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_username.setObjectName("label_username")
        self.gridLayout.addWidget(self.label_username, 1, 0, 1, 1)
        self.label_chatport = QtWidgets.QLabel(TorMessengerLogin)
        self.label_chatport.setObjectName("label_chatport")
        self.gridLayout.addWidget(self.label_chatport, 4, 0, 1, 1)
        self.label_serverurl = QtWidgets.QLabel(TorMessengerLogin)
        self.label_serverurl.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_serverurl.setObjectName("label_serverurl")
        self.gridLayout.addWidget(self.label_serverurl, 3, 0, 1, 1)
        self.TorPort = QtWidgets.QLineEdit(TorMessengerLogin)
        self.TorPort.setObjectName("TorPort")
        self.gridLayout.addWidget(self.TorPort, 4, 4, 1, 1)
        self.TorPort.setText(self.usrInfo[3])
        self.LoginButton = QtWidgets.QPushButton(TorMessengerLogin)
        self.LoginButton.setObjectName("LoginButton")
        self.gridLayout.addWidget(self.LoginButton, 7, 0, 1, 5)
        self.LoginButton.clicked.connect(self.LoginMe)
        self.LoginButton.raise_()
        self.label_username.raise_()
        self.label_password.raise_()
        self.label_status.raise_()
        self.label_serverurl.raise_()
        self.Password.raise_()
        self.ServerURL.raise_()
        self.UserName.raise_()
        self.ChatPort.raise_()
        self.label_chatport.raise_()
        self.label_torport.raise_()
        self.TorPort.raise_()

        self.retranslateUi(TorMessengerLogin)
        QtCore.QMetaObject.connectSlotsByName(TorMessengerLogin)
        

    
    ## Called when login/enter is pressed
    def LoginMe(self):
        _translate = QtCore.QCoreApplication.translate
        if self.Password.text() == "":
            self.label_status.setText("Status: Invalid Password Length.")
            QApplication.processEvents()
        else:
            self.label_status.setText(_translate("TorMessengerLogin", "Status: Connecting."))
            QApplication.processEvents()
            self.userInfo = [self.UserName.text(), self.ServerURL.text(), self.ChatPort.text(), self.TorPort.text()]
            self.name = self.userInfo[0]
            self.password = self.Password.text()
            if self.TOR.isChecked():
                self.userInfo.append('torTrue')
                self.usrSave()
                self.torme = "y"
                #self.start()
                self.start()
                
            else:
                self.usrSave()
                self.torme = "n"
                #self.start()
                self.start()
            
    def retranslateUi(self, TorMessengerLogin):
        _translate = QtCore.QCoreApplication.translate
        TorMessengerLogin.setWindowTitle(_translate("TorMessengerLogin", "HolistiChat by Holistic Coding | Login"))
        self.label_torport.setText(_translate("TorMessengerLogin", "Tor Port"))
        self.label_password.setText(_translate("TorMessengerLogin", "Password: "))
        self.label_status.setText(_translate("TorMessengerLogin", "Status: Waiting."))
        self.label_username.setText(_translate("TorMessengerLogin", "Username:"))
        self.label_chatport.setText(_translate("TorMessengerLogin", "Chat Port"))
        self.label_serverurl.setText(_translate("TorMessengerLogin", "Server URL:"))
        self.LoginButton.setText(_translate("TorMessengerLogin", "Login"))
        self.TOR.setText(_translate("TorMessengerLogin", "Use Tor"))
        
        
    def eventFilter(self, obj, event):
        if event.type() == QtCore.QEvent.KeyPress and obj is self.CommentBox:
            if event.key() == QtCore.Qt.Key_Return and self.CommentBox.hasFocus():
                self.main()
        return super().eventFilter(obj, event)


    ## QT Chat Area
    def setupUi2(self, TorMessenger):
        TorMessenger.setObjectName("HolistiChat by Holistic Coding")
        TorMessenger.resize(920, 636)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("incl/logo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        TorMessenger.setWindowIcon(icon)
        TorMessenger.setStyleSheet("")
        self.centralwidget = QtWidgets.QWidget(TorMessenger)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.OnlineUserList = QtWidgets.QListWidget(self.centralwidget)
        self.OnlineUserList.setMinimumSize(QtCore.QSize(200, 0))
        self.OnlineUserList.setMaximumSize(QtCore.QSize(200, 16777215))
        self.OnlineUserList.setStyleSheet("")
        self.OnlineUserList.setObjectName("OnlineUserList")
        self.gridLayout.addWidget(self.OnlineUserList, 0, 0, 1, 1)
        self.SendButton = QtWidgets.QPushButton(self.centralwidget)
        self.SendButton.setMaximumSize(QtCore.QSize(100, 40))
        self.SendButton.setStyleSheet("")
        icon = QtGui.QIcon.fromTheme("Breeze")
        self.SendButton.setIcon(icon)
        self.SendButton.setObjectName("SendButton")
        self.gridLayout.addWidget(self.SendButton, 1, 2, 1, 1)
        self.SendButton.clicked.connect(self.main)
        self.CommentBox = QtWidgets.QLineEdit(TorMessengerLogin)
        self.CommentBox.setMaximumSize(QtCore.QSize(16777215, 40))
        self.CommentBox.setStyleSheet("")
        self.CommentBox.setObjectName("CommentBox")
        self.CommentBox.returnPressed.connect(self.SendButton.click)
        self.CommentBox.scroll(10000000 ,100000000)
        self.CommentBox.show()
        if QtCore.Qt.Key_Enter == True:
            self.main()
        self.gridLayout.addWidget(self.CommentBox, 1, 0, 1, 2)
        self.ChatWindow = QtWidgets.QTextBrowser(self.centralwidget)
        self.ChatWindow.setStyleSheet("")
        self.ChatWindow.setObjectName("ChatWindow")
        
        self.gridLayout.addWidget(self.ChatWindow, 0, 1, 1, 2)
        TorMessenger.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(TorMessenger)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 920, 22))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        TorMessenger.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(TorMessenger)
        self.statusbar.setObjectName("statusbar")
        TorMessenger.setStatusBar(self.statusbar)
        self.actionOptions = QtWidgets.QAction(TorMessenger)
        self.actionOptions.setObjectName("actionOptions")
        self.actionExit = QtWidgets.QAction(TorMessenger)
        self.actionExit.setObjectName("actionExit")
        self.actionCommands = QtWidgets.QAction(TorMessenger)
        self.actionCommands.setObjectName("actionCommands")
        self.actionEdit = QtWidgets.QAction(TorMessenger)
        self.actionEdit.setObjectName("actionEdit")
        self.menuFile.addAction(self.actionOptions)
        self.menuFile.addAction(self.actionExit)
        self.menuHelp.addAction(self.actionCommands)
        self.menuHelp.addAction(self.actionEdit)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi2(TorMessenger)
        QtCore.QMetaObject.connectSlotsByName(TorMessenger)

    def retranslateUi2(self, TorMessenger):
        _translate = QtCore.QCoreApplication.translate
        TorMessenger.setWindowTitle(_translate("TorMessenger", "HolistiChat by Holistic Coding", "Chat"))
        self.SendButton.setText(_translate("TorMessenger", "Send"))
        self.menuFile.setTitle(_translate("TorMessenger", "File"))
        self.menuHelp.setTitle(_translate("TorMessenger", "Help"))
        self.actionOptions.setText(_translate("TorMessenger", "Options"))
        self.actionExit.setText(_translate("TorMessenger", "Exit"))
        self.actionCommands.setText(_translate("TorMessenger", "Commands"))
        self.actionEdit.setText(_translate("TorMessenger", "Edit"))

        
    def startme(self):
        pass
        
        
if __name__ == "__import__":
    import sys
    from PyQt5.QtCore import *
    from PyQt5.QtGui import *
    from PyQt5.QtWidgets import *
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle("Fusion")
    dark_palette = QPalette()
    dark_palette.setColor(QPalette.Window, QColor(53, 53, 53))
    dark_palette.setColor(QPalette.WindowText, Qt.white)
    dark_palette.setColor(QPalette.Base, QColor(25, 25, 25))
    dark_palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
    dark_palette.setColor(QPalette.ToolTipBase, Qt.white)
    dark_palette.setColor(QPalette.ToolTipText, Qt.white)
    dark_palette.setColor(QPalette.Text, Qt.white)
    dark_palette.setColor(QPalette.Button, QColor(53, 53, 53))
    dark_palette.setColor(QPalette.ButtonText, Qt.white)
    dark_palette.setColor(QPalette.BrightText, Qt.red)
    dark_palette.setColor(QPalette.Link, QColor(42, 130, 218))
    dark_palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
    dark_palette.setColor(QPalette.HighlightedText, Qt.black)
    app.setPalette(dark_palette)
    app.setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }")
    TorMessengerLogin = QtWidgets.QDialog()
    TorMessenger = QtWidgets.QMainWindow()
    Torchat.CommentBox.installFilter(TorMessenger)
    ui2 = Torchat()
    ui2.setupUi2(TorMessenger)
    #TorMessenger.show()
    ui = Torchat()
    ui.setupUi(TorMessengerLogin)
    left_chat = "User " + Torchat.name + " Left."
    Torchat.s.send(left_chat.encode('utf-8'))
    Torchat.s.close()
    sleep(2)
    sys.exit(app.exec_())

        
'''        
######################################################################################################
#  Main Chat GUI                                                                                         #
######################################################################################################
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPalette, QColor
'''



        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        









